package de.drentech.teacherdocs.dataplatform.common.health;

import de.drentech.teacherdocs.dataplatform.common.data.DatabaseConnection;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.health.Health;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.SQLException;

@Health
@ApplicationScoped
public class DatabaseHealthCheck implements HealthCheck {

    @ConfigProperty(name = "database.up", defaultValue = "false")
    private boolean databaseUp;

    @Inject
    private DatabaseConnection databaseConnection;

    @Override
    public HealthCheckResponse call() {
        HealthCheckResponseBuilder responseBuilder = HealthCheckResponse.named("database-check");

        try {
            checkDatabaseConnection();
            responseBuilder.up();
        } catch (IllegalStateException | SQLException e) {
            // cannot access the database
            responseBuilder.down()
                    .withData("error", e.getMessage());
        }

        return responseBuilder.build();
    }

    private void checkDatabaseConnection() throws SQLException {
        if (!databaseUp) {
            throw new IllegalStateException("Cannot contact database");
        }
        this.databaseConnection.getDatabaseConnection();
    }
}
