package de.drentech.teacherdocs.dataplatform.common.data;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@ApplicationScoped
public class DatabaseConnection {

    @ConfigProperty(name = "quarkus.datasource.url")
    private String databaseUrl;

    @ConfigProperty(name = "quarkus.datasource.username")
    private String databaseUser;

    @ConfigProperty(name = "quarkus.datasource.password")
    private String databasePassword;

    public Connection getDatabaseConnection() throws SQLException {
        Connection connection = DriverManager.getConnection(databaseUrl, databaseUser, databasePassword);

        return connection;
    }
}
