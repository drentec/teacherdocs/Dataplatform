package de.drentech.teacherdocs.dataplatform.common.health;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.health.Health;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;

import javax.enterprise.context.ApplicationScoped;
import java.lang.annotation.Annotation;

@Health
@ApplicationScoped
public class ApplicationHealthCheck implements HealthCheck {

    @ConfigProperty(name = "application.up", defaultValue = "false")
    private boolean applicationUp;

    @Override
    public HealthCheckResponse call() {
        HealthCheckResponseBuilder responseBuilder = HealthCheckResponse.named("application-check");

        if(applicationUp) {
            return responseBuilder.up().build();
        } else {
            return responseBuilder.down().build();
        }
    }
}
